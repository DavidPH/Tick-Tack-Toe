import pygame
import sys

pygame.init()
pygame.font.init()

black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
orange = (255, 164, 38)
light_blue = pygame.Color('dodgerblue2')
blue = (0, 0, 255)
grey = (141, 138, 142)
dark_green = (15, 130, 80)
font = pygame.font.SysFont('Comic Sans MS', 35, True, True)
big_font = pygame.font.SysFont('Comic Sans MS', 65, True)
display_width = 500
display_height = 500
clock = pygame.time.Clock()
gameOver = False
game_exit = False
players = []
icon = pygame.image.load('logo.png')
pygame.display.set_icon(icon)
cross_img = pygame.image.load('Cross.png')
circle_img = pygame.image.load('Circle.png')
strike_img = pygame.image.load('Strike.png')
exit_img = [pygame.image.load('Exit.png'), pygame.image.load('Exit_Pressed.png')]
repeat_img = [pygame.image.load('Repeat.png'), pygame.image.load('Repeat_Pressed.png')]
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("Tick Tack Toe")


def make_button(image, coords):
    image_rect = image.get_rect()
    image_rect.center = coords
    return image_rect


class InputBox:
    def __init__(self, x, y, w, h, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.color = grey
        self.text = text
        self.txt_surface = font.render(text, True, self.color)
        self.active = False

    def get_text(self):
        return self.text

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            # If the user clicked on the input_box rect.
            if self.rect.collidepoint(event.pos):
                # Toggle the active variable.
                self.active = not self.active
                self.text = ''
                self.txt_surface = font.render(self.text, True, self.color)
            else:
                self.active = False

            # Change the current color of the input box.
            self.color = light_blue if self.active else grey
        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_RETURN:
                    self.color = grey
                    self.active = False
                    return "enter"

                elif event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    self.text += event.unicode
                # Re-render the text.
                self.txt_surface = font.render(self.text, True, self.color)
            if event.key == pygame.K_RETURN:
                self.color = grey
                self.active = False
                return "enter"

    def update(self):
        # Resize the box if the text is too long.
        width = max(200, self.txt_surface.get_width()+10)
        self.rect.w = width

    def draw(self, screen):
        # Blit the text.
        screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
        # Blit the rect.
        pygame.draw.rect(screen, self.color, self.rect, 2)


class Board:
    x = 3
    y = 3

    def __init__(self):
        self.array = [["NA" for _ in range(self.x)] for _ in range(self.y)]

    def draw_board(self):
        gameDisplay.fill(white)
        pygame.draw.rect(gameDisplay, black, [display_width / self.x, 0, 2, 500], 4)
        pygame.draw.rect(gameDisplay, black, [display_width * 2 / self.x, 0, 2, 500], 4)
        pygame.draw.rect(gameDisplay, black, [0, display_height / self.y, 500, 2], 4)
        pygame.draw.rect(gameDisplay, black, [0, display_height * 2 / self.y, 500, 2], 4)
        pygame.display.update()

    def check_pos(self, turn):
        pos = pygame.mouse.get_pos()
        coordinate = []
        if 0 <= pos[0] < display_width / 3:
            coordinate.append(0)
        elif display_width * 2 / 3 > pos[0] > display_width / 3:
            coordinate.append(1)
        elif display_width * 2 / 3 < pos[0] < display_width:
            coordinate.append(2)
        if 0 <= pos[1] < display_height / 3:
            coordinate.append(0)
        elif display_height * 2 / 3 > pos[1] > display_height / 3:
            coordinate.append(1)
        elif display_height * 2 / 3 < pos[1] < display_height:
            coordinate.append(2)
        valid = False
        if coordinate[0] == 0:
            if coordinate[1] == 0:
                if self.array[0][0] == "NA":
                    self.array[0][0] = turn
                    valid = True
            if coordinate[1] == 1:
                if self.array[1][0] == "NA":
                    self.array[1][0] = turn
                    valid = True
            if coordinate[1] == 2:
                if self.array[2][0] == "NA":
                    self.array[2][0] = turn
                    valid = True

        elif coordinate[0] == 1:
            if coordinate[1] == 0:
                if self.array[0][1] == "NA":
                    self.array[0][1] = turn
                    valid = True
            if coordinate[1] == 1:
                if self.array[1][1] == "NA":
                    self.array[1][1] = turn
                    valid = True
            if coordinate[1] == 2:
                if self.array[2][1] == "NA":
                    self.array[2][1] = turn
                    valid = True

        elif coordinate[0] == 2:
            if coordinate[1] == 0:
                if self.array[0][2] == "NA":
                    self.array[0][2] = turn
                    valid = True
            if coordinate[1] == 1:
                if self.array[1][2] == "NA":
                    self.array[1][2] = turn
                    valid = True
            if coordinate[1] == 2:
                if self.array[2][2] == "NA":
                    self.array[2][2] = turn
                    valid = True

        if valid:
            players[Player.turn].latest_coord = coordinate

        return valid

    def draw_symbol(self, symbol, coordinate):
        x = display_width / 3
        y = display_height / 3
        if coordinate == [0, 0]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (0, 0))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (0, 0))
        elif coordinate == [1, 0]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x, 0))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x, 0))
        elif coordinate == [2, 0]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x * 2, 0))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x * 2, 0))
        elif coordinate == [0, 1]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (0, y))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (0, y))
        elif coordinate == [1, 1]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x, y))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x, y))
        elif coordinate == [2, 1]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x * 2, y))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x * 2, y))
        elif coordinate == [0, 2]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (0, y * 2))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (0, y * 2))
        elif coordinate == [1, 2]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x, y * 2))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x, y * 2))
        elif coordinate == [2, 2]:
            if symbol == 'X':
                gameDisplay.blit(cross_img, (x * 2, y * 2))
            elif symbol == 'O':
                gameDisplay.blit(circle_img, (x * 2, y * 2))
        pygame.display.update()


class Player:
    turn = 0

    def __init__(self, name, symbol):
        self.name = name
        self.symbol = symbol
        self.latest_coord = (-1, -1)
        self.score = 0

    def score_increase(self):
        self.score += 1

    @classmethod
    def next_turn(cls):
        if cls.turn == 0:
            cls.turn = 1
        else:
            cls.turn = 0

        return cls.turn


def check(array, symbol):
    global gameOver
    w = display_width / 3
    h = display_height / 3
    # check horizontally:
    for i in range(3):
        count = 0
        for x in array[i]:
            if x == symbol:
                count += 1
            if count == 3:
                gameOver = True
                if i == 0:
                    gameDisplay.blit(pygame.transform.rotate(strike_img, 90), (w / 3.5, (h / 2.4)))
                elif i == 1:
                    gameDisplay.blit(pygame.transform.rotate(strike_img, 90), (w / 3.5, (h / .7)))
                elif i == 2:
                    gameDisplay.blit(pygame.transform.rotate(strike_img, 90), (w / 3.5, (h * 3 / 2 * 1.6)))

                pygame.display.update()
                break

    # check vertically:
    for i in range(3):
        count = 0
        for j in range(3):
            if array[j][i] == symbol:
                count += 1
                if count == 3:
                    gameOver = True
                    if i == 0:
                        gameDisplay.blit(strike_img, ((w / 2), h / 3.5))
                    elif i == 1:
                        gameDisplay.blit(strike_img, ((w * 3) / 2, h / 3.5))
                    elif i == 2:
                        gameDisplay.blit(strike_img, ((w * 3) - (w / 2), h / 3.5))
                    pygame.display.update()
                    break

    # check diagonally:
    count = 0
    for i in range(3):
        if array[i][i] == symbol:
            count += 1
            if count == 3:
                gameOver = True
                gameDisplay.blit(pygame.transform.rotozoom(strike_img, 48, 1.2), (w / 3.2, (h / 2.4)))
                pygame.display.update()
                break
    count = 0
    j = 2
    for i in range(3):
        if array[j][i] == symbol:
            count += 1
            if count == 3:
                gameOver = True
                gameDisplay.blit(pygame.transform.rotozoom(strike_img, 312, 1.2), ((w / 3.2), (h / 2.4)))
                pygame.display.update()
                break
        j -= 1

    # check for tie:
    count = 0
    if not gameOver:
        for i in range(3):
            for j in range(3):
                if array[i][j] != "NA":
                    count += 1
    if count == 9:
        gameOver = True
        return True
    else:
        return False


def make_win_screen(player, tie):
    global game_exit
    global gameOver
    win_screen = True
    pygame.time.wait(200)
    win_surface = pygame.Surface((display_width, display_height), pygame.SRCALPHA)
    win_surface.fill((255, 255, 255, 0))
    translucent_surface = pygame.Surface((display_width, display_height), pygame.SRCALPHA)
    translucent_surface.fill((255, 255, 255, 200))
    gameDisplay.blit(translucent_surface, (0, 0))
    pygame.display.update()
    player_1 = font.render("{}: {}".format(players[0].name, players[0].score), True, black)
    player_2 = font.render("{}: {}".format(players[1].name, players[1].score), True, black)
    if tie:
        win_text = big_font.render("It's a tie!", True, orange)
    else:
        win_text = big_font.render("{} wins!".format(player.name), True, dark_green)
    text_rect = win_text.get_rect()
    text_rect.center = (display_width / 2), (display_height/3)
    win_surface.blit(win_text, text_rect)
    exit_rect = make_button(exit_img[0], (display_width//3, display_height*2//3))
    repeat_rect = make_button(repeat_img[0], (display_width*2//3, display_height*2//3))
    win_surface.blit(exit_img[0], exit_rect)
    win_surface.blit(repeat_img[0], repeat_rect)
    if players[0].score > players[1].score:
        win_surface.blit(player_1, (0, 0))
        win_surface.blit(player_2, (0, 50))
    else:
        win_surface.blit(player_2, (0, 0))
        win_surface.blit(player_1, (0, 50))
    gameDisplay.blit(win_surface, (0, 0))
    pygame.display.update()

    repeat = False
    leave = False
    while win_screen:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse = pygame.mouse.get_pos()
                if exit_rect.collidepoint(mouse):
                    win_surface.blit(exit_img[1], exit_rect)
                    gameDisplay.blit(win_surface, (0, 0))
                    pygame.display.update()
                    leave = True

                if repeat_rect.collidepoint(mouse):
                    win_surface.blit(repeat_img[1], repeat_rect)
                    gameDisplay.blit(win_surface, (0, 0))
                    pygame.display.update()
                    repeat = True

            if event.type == pygame.MOUSEBUTTONUP:
                mouse = pygame.mouse.get_pos()
                win_surface.blit(exit_img[0], exit_rect)
                win_surface.blit(repeat_img[0], repeat_rect)
                gameDisplay.blit(win_surface, (0, 0))
                pygame.display.update()
                if repeat and repeat_rect.collidepoint(mouse):
                    win_screen = False
                    gameOver = False
                    game_loop()
                elif leave and exit_rect.collidepoint(mouse):
                    sys.exit()
        clock.tick(30)


def start_screen():
    global players
    start = True
    gameDisplay.fill(white)

    start_img = [pygame.image.load('Start.png'), pygame.image.load('Start_Pressed.png')]
    start_rect = make_button(start_img[0], (display_width//2, display_height*2//3))
    gameDisplay.blit(start_img[0], start_rect)

    player1_entry = InputBox(150, 70, 180, 60, "Player 1")
    player2_entry = InputBox(150, 140, 180, 60, "Player 2")
    input_boxes = [player1_entry, player2_entry]
    pygame.display.update()
    pressed_enter = False
    while start:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            for box in input_boxes:
                result = box.handle_event(event)
                if result == "enter":
                    pressed_enter = True
                box.update()
                box.draw(gameDisplay)
            if event.type == pygame.MOUSEBUTTONDOWN or pressed_enter:
                mouse = pygame.mouse.get_pos()
                if start_rect.collidepoint(mouse) or pressed_enter:
                    gameDisplay.blit(start_img[1], start_rect)
                    pygame.display.update()
                    pygame.time.wait(100)
            if event.type == pygame.MOUSEBUTTONUP:
                mouse = pygame.mouse.get_pos()
                gameDisplay.blit(start_img[0], start_rect)
                pygame.display.update()
                if start_rect.collidepoint(mouse):
                    pressed_enter = True
            if pressed_enter:
                if player1_entry.get_text() != "" and player2_entry.get_text() != "":
                    players = [Player(player1_entry.get_text(), "X"), Player(player2_entry.get_text(), "O")]
                    start = False
                    game_loop()
                    pygame.time.wait(80)
                else:
                    pressed_enter = False
            gameDisplay.fill(white)
            for box in input_boxes:
                box.draw(gameDisplay)
            gameDisplay.blit(start_img[0], start_rect)
            pygame.display.update()
        clock.tick(30)


def game_loop():
    global game_exit
    game_exit = False
    board = Board()
    board.draw_board()
    i = 0
    while not game_exit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN and not gameOver:
                valid = board.check_pos(players[i].symbol)
                if valid:
                    board.draw_symbol(players[i].symbol, players[i].latest_coord)
                    tie = check(board.array, players[i].symbol)
                    if gameOver:
                        if tie:
                            make_win_screen(players[i], True)
                        else:
                            players[i].score_increase()
                            make_win_screen(players[i], False)
                    i = Player.next_turn()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    game_loop()
                    pygame.time.wait(280)
        clock.tick(30)


start_screen()
pygame.quit()

quit()
